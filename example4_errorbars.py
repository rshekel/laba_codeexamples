import numpy as np 
import matplotlib.pyplot as plt 


H = np.array([1.1, 1.2, 1.3, 1.8]) 
r = np.array([10, 15, 20, 30])

H_errs = 0.05  # constant error
r_errs = np.array([0.4, 0.6, 0.8, 0.2])  # Error per each measurement 

fig, ax = plt.subplots()
ax.errorbar(H, r, yerr=r_errs, xerr=H_errs, fmt='.')
ax.set_xlabel('H (cm)') 
ax.set_ylabel('r ($ \mu m $)') 
fig.show()

plt.show() # Make sure python won't exit and close figure immediately 
