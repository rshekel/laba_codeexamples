import numpy as np 
import matplotlib.pyplot as plt 

path = r"C:\HUJI\Year1\LabA\EnergyExperiment\Data\data1.csv"
path2 = "C:\\temp\\data1.csv"

data = np.loadtxt(path2, skiprows=1, delimiter=',')

H_cm = data[:, 0]  # All rows, first column (0 is the first) 
H = H_cm / 100 # now H in meters 
r =  data[:, 1]  # Second column (1 is the second column)
    

fig, ax = plt.subplots()
ax.plot(H, r, '*')
ax.set_xlabel('H (cm)') 
ax.set_ylabel('r ($ \mu m $)') # in between $ $ you can enter things like \alpha or \theta etc. (latex syntax) 
fig.show()

plt.show() # Make sure python won't exit and close figure immediately 
