import numpy as np 
import matplotlib.pyplot as plt 


H = np.array([1.1, 1.2, 1.3, 1.8])  # This is a comment 
r = np.array([10, 15, 20, 30])


fig, ax = plt.subplots()
ax.plot(H, r, '*')  # can use instead of '*' also '.' or '--' or '-' etc.
ax.set_xlabel('H (cm)') 
ax.set_ylabel('r (cm)')
fig.show()

plt.show() # Make sure python won't exit and close figure immediately 
