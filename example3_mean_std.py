import numpy as np

X = np.array([0.2, 0.4, 0.33, 0.38, 0.5, 0.44])
N = len(X)
mean_X = np.mean(X)
std_X = np.std(X, ddof=1)
real_uncertainty = std_X / np.sqrt(N)

print(f'expected measured value is {mean_X} +- {std_X:.4f}')  # :.4f will print 4 numbers after the .
print(f'real value is {mean_X} +- {real_uncertainty:.4f}')
