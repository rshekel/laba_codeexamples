import numpy as np 
import matplotlib.pyplot as plt 
from curvefitting import cftool

# Create a noisy signal 
X = np.linspace(0, 8, 50)  # 500 data points between 0 and 8 
Y = np.sin(X) + np.random.uniform(0, 0.2, len(X)) 

cftool(globals())
# cftool({'X': X, 'Y': Y})
