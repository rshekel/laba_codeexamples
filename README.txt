To work with python:

* Download latest version of python from here: https://www.python.org/downloads/
    - I Recommend installing it not to the suggested path in program files, but rather in C:\Python39 (You will be prompted for this during installation)

* After you have python installed you will need to install physics related packages. 
    - Open the command propmt(CMD) and type:
        - pip install numpy
        - pip install matplotlib
        - pip install ipython
        - pip install scipy 
        - pip install curvefitting
    - Each of these will take a few minutes to finish... 

* Install Pycharm community version from here: https://www.jetbrains.com/pycharm/download/#section=windows
    - You can also install the professional edition with educational license, which isn't very hard, but I don't feel like writing a manual for that right now :P 
