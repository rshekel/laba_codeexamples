import numpy as np 
import matplotlib.pyplot as plt 
from scipy.optimize import curve_fit

# Create a noisy signal 
X = np.linspace(0, 8, 50)  # 500 data points between 0 and 8 
Y = np.sin(X) + np.random.uniform(0, 0.2, len(X)) 


# defining a function that we will want to fit to...
def my_sin(x, a, w, c, phi):
    return a * np.sin(w * x + phi) + c

# Actual fitting process 
popt, pcov = curve_fit(my_sin, X, Y)  # fitting the data in X and Y to find the correct parameters for the function my_sin 
a, w, c, phi = popt
errs = np.sqrt(np.diag(pcov))  # pcov is the covariance - learn about it another day...  

print('fit result for a*sin(w*x + phi) + c')
print(f'A: {a} +- {errs[0]}')     
print(f'omega: {w} +- {errs[1]}')     
print(f'c: {c} +- {errs[2]}')     
print(f'phi: {phi} +- {errs[3]}')     

#### PLOT ####
fig, ax = plt.subplots()
ax.plot(X, Y, '*', label='orig data')

# Plot the fit 
dummy_X = np.linspace(0, 8, 1000)
fitted_Y = my_sin(dummy_X, a, w, c, phi)
ax.plot(dummy_X, fitted_Y, '--', label='fit to sin')
ax.legend()

ax.set_xlabel('time (years)') 
ax.set_ylabel('temperature (C)')
fig.show()

plt.show()
